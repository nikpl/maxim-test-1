﻿using Autofac;
using Maxim1.TextProcessing;
using Maxim1.Translation;
using Maxim1.Translation.YandexTranslator;
using System;
using System.Threading.Tasks;

namespace Maxim1
{
    class Program
    {
        private static TextProcessorChain processorChain;
        private static IContainer container;

        static void Main(string[] args)
        {
            InitializeContainer();
            CreateProcessorChain();
            CreateProcessors();
            StartProcessing().Wait();
            Console.ReadKey();
        }

        private static void InitializeContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<HardCodedApiKeyProvider>().As<IYandexApiKeyProvider>();
            builder.RegisterType<YandexTranslator>().As<ITranslator>();
            builder.RegisterType<TranslateProcessor>().AsSelf();
            container = builder.Build();
        }

        private static void CreateProcessorChain()
        {
            processorChain = new TextProcessorChain
            {
                TextInput = new ConsoleInput(),
                TextOutput = new ConsoleOutput()
            };
        }

        private static void CreateProcessors()
        {
            var translationProcessor = CreateRussianToEnglishTranslateProcessor();
            processorChain.AddTextProcessor(translationProcessor);
        }

        private static ITextProcessor CreateRussianToEnglishTranslateProcessor()
        {
            var tp = container.Resolve<TranslateProcessor>();
            tp.TranslateFrom = Language.Russian;
            tp.TranslateTo = Language.English;
            return tp;
        }

        private async static Task StartProcessing()
        {
            try
            {
                await ProcessingLoop();
            }
            catch(TextProcessorException)
            {
                // Нормальное завершение
            }
            catch(Exception e)
            {
                Console.WriteLine("Критическая ошибка:");
                Console.WriteLine(e.Message);
            }
        }
        private async static Task ProcessingLoop()
        {
            while (true)
            {
                await processorChain.Process();
            }
        }
    }
}
