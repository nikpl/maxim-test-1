﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Maxim1.TextProcessing
{
    interface ITextProcessor
    {
        Task<string> Process(string input);
    }
}
