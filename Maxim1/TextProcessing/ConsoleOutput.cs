﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Maxim1.TextProcessing
{
    class ConsoleOutput : ITextOutput
    {
        public async Task Output(string text)
        {
            Console.WriteLine(text);
        }
    }
}
