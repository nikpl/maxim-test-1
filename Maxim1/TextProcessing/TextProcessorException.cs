﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maxim1.TextProcessing
{
    class TextProcessorException: Exception
    {
        public TextProcessorException(string message, bool stopProcessing = false): base(message)
        {
            StopProcessing = stopProcessing;
        }

        public bool StopProcessing { get; }
    }
}
