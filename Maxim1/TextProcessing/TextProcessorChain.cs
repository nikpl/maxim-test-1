﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Maxim1.TextProcessing
{
    class TextProcessorChain
    {
        private List<ITextProcessor> textProcessors = new List<ITextProcessor>();

        public ITextInput TextInput { get; set; }
        public ITextOutput TextOutput { get; set; }

        public void AddTextProcessor(ITextProcessor textProcessor)
        {
            textProcessors.Add(textProcessor);
        }

        public async Task Process()
        {
            try
            {
                var currentText = await TextInput.Input();
                foreach (var processor in textProcessors)
                    currentText = await processor.Process(currentText);
                await TextOutput.Output(currentText);
            }
            catch(TextProcessorException e)
            {
                TextOutput.Output(e.Message).Wait();
                if (e.StopProcessing)
                    throw;
            }
        }
    }
}
