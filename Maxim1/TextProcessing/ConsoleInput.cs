﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Maxim1.TextProcessing
{
    internal class ConsoleInput: ITextInput
    {
        public async Task<string> Input()
        {
            Console.WriteLine("Введите текст для переовода (для выхода введите /q):");
            var input = Console.ReadLine();
            if (input == "/q")
                throw new TextProcessorException("Выход из приложения", true);
            return input;
        }
    }
}
