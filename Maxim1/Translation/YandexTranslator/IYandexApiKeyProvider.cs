﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maxim1.Translation.YandexTranslator
{
    public interface IYandexApiKeyProvider
    {
        string GetKey();
    }
}
