﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maxim1
{
    class YandexTranslatorResponseDto
    {
        public int Code { get; set; }
        public string Lang { get; set; }
        public string[] Text { get; set; }
    }
}
