﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Maxim1.Translation.YandexTranslator
{
    internal class YandexTranslator: ITranslator
    {
        private static readonly IReadOnlyDictionary<Language, string> LANGUAGE_KEYS = new Dictionary<Language, string>
        {
            { Language.English,  "en" },
            { Language.Russian,  "ru" },
        };
        private static readonly string BASE_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";
        private static readonly string TEXT_PARAM = "text";
        private static readonly string LANG_PARAM = "lang";
        private static readonly string APIKEY_PARAM = "key";

        private string apiKey;

        public YandexTranslator(IYandexApiKeyProvider apiKeyProvider)
        {
            this.apiKey = apiKeyProvider.GetKey();
        }

        public async Task<string> Translate(string text, Language from, Language to)
        {
            var langPair = GetLanguagePairDescription(from, to);
            var url = BuildUrl(text, langPair);
            HttpClient httpClient = new HttpClient();
            var result = await httpClient.GetStringAsync(url);
            var resultObj = JsonConvert.DeserializeObject<YandexTranslatorResponseDto>(result);
            return String.Join(Environment.NewLine, resultObj.Text);
        }

        private string BuildUrl(string text, string languagePair)
        {
            return $"{BASE_URL}?{APIKEY_PARAM}={apiKey}&{LANG_PARAM}={languagePair}&{TEXT_PARAM}={text}";
        }

        private string GetLanguagePairDescription(Language from, Language to)
        {
            try
            {
                return $"{LANGUAGE_KEYS[from]}-{LANGUAGE_KEYS[to]}";
            }
            catch (KeyNotFoundException)
            {
                throw new NotImplementedException();
            }
            
        }
    }
}
