﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maxim1.Translation.YandexTranslator
{
    internal class HardCodedApiKeyProvider : IYandexApiKeyProvider
    {
        public string GetKey()
        {
            return "trnsl.1.1.20180912T102217Z.dc935c8e67974b95.07b971ced9eb99157812d80b129dc8a7fbeb5bd4";
        }
    }
}
