﻿using Maxim1.TextProcessing;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Maxim1.Translation
{
    class TranslateProcessor : ITextProcessor
    {
        private ITranslator translator;

        public TranslateProcessor(ITranslator translator)
        {
            this.translator = translator;
        }

        public Language TranslateFrom { get; set; }
        public Language TranslateTo { get; set; }

        public async Task<string> Process(string input)
        {
            return await translator.Translate(input, TranslateFrom, TranslateTo);
        }
    }
}
