﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Maxim1.Translation
{
    public interface ITranslator
    {
        Task<string> Translate(string text, Language from, Language to);
    }

    public enum Language
    {
        Russian = 1,
        English = 2
    }
}
